# DEMO 2 - Static Web Distribution

![](demo2.png)

## Host web on Nginx
```bash
docker run -it --rm -p 8080:80 --name web -v ${PWD}:/usr/share/nginx/html nginx
```

## Host web on Nginx With docker-compose
```bash
docker-compose up
```

## Docker Build
```bash
docker build -t [repository_url]/[image_name]:[image_tag] .

#example:
docker build -t donnyrhomanzah/mars-landing:0.1 .
```

## Docker Push 
```bash
docker push [repository_url]/[image_name]:[image_tag]

#example:
docker push donnyrhomanzah/mars-landing:1.0
```