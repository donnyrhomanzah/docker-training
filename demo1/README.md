# DEMO 1 - MYSQL Server

![](demo1.png)

## Running MySQL
```bash
docker run -d --name latihan-mysql -e MYSQL_ROOT_PASSWORD=root mysql
```

## Running MySQL with Port Expose
```bash
docker run -d --name latihan-mysql -e MYSQL_ROOT_PASSWORD=root -p 3300:3306 mysql
```

## Running MySQL with Volume Biding (Persistent Data)
```bash
docker run -d --name latihan-mysql -e MYSQL_ROOT_PASSWORD=root -v ${PWD}/data:/var/lib/mysql -p 3300:3306 mysql
```

